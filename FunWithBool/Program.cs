﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithBool {
    class Program {
        static unsafe void Main(string[] args) {
            bool b1 = true;
            bool b2 = true;

            *(int*)&b2 = 10;

            Console.WriteLine($"b1({b1}) == true => {b1 == true}");
            Console.WriteLine($"b2({b1}) == true => {b1 == true}");
            Console.WriteLine($"b1({b1}) == b2({b2}) => {b1 == b2}");
            Console.WriteLine($"b1({b1}) && b2({b2}) => {b1 && b2}");

            Console.ReadLine();
        }
    }
}
